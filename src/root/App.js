import React, { Component } from 'react';
import { Admin, Resource } from 'react-admin';
import dashboard from '../functionality/dashboard';
import authProvider from '../functionality/authProvider';
import dataProvider from '../config/urlAPI'
import user from '../functionality/user';
import exam from '../functionality/exam';
import subject from '../functionality/subject';
import thematic from '../functionality/thematic';
import question from '../functionality/question';
import systemRoom from '../functionality/systemRoom';
import history from '../functionality/history';

import { createMuiTheme } from '@material-ui/core/styles';
import indigo from '@material-ui/core/colors/indigo';
import red from '@material-ui/core/colors/red';
import blue from '@material-ui/core/colors/blue';

import polyglotI18nProvider from 'ra-i18n-polyglot';
import vietnameseMessages from './ra-language-vietnamese';

const myTheme = createMuiTheme({
  palette: {
    primary: indigo, // text
    secondary: blue, // title
    error: red, // error
    contrastThreshold: 3, // độ tương phản
    tonalOffset: 0.2, // tông màu 
  },
  typography: {
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Arial',
      'sans-serif',
    ].join(','),
  },
  overrides: {
    // MuiDrawer: { // override the styles of all instances of this component
    //   docked: {  // Name of the rule
    //     "overflow-y": "scroll",
    //     "maxHeight": "80vh",
    //     "&::-webkit-scrollbar": { "width": "10px" },
    //     "&::-webkit-scrollbar-track": { "background": "red" }, // #f1f1f1
    //     "&::-webkit-scrollbar-thumb": { "background": "blue" }, // #888
    //   }
    // }  
  },
});

const i18nProvider = polyglotI18nProvider(() => vietnameseMessages, 'vi');

class App extends Component {
  render() {
    return (
      <Admin
        i18nProvider={i18nProvider}
        dashboard={dashboard}
        authProvider={authProvider}
        dataProvider={dataProvider}
        theme={myTheme}
      >
        {permissions => [
          permissions === 'admin' && 
            <Resource options={{ label: 'User' }} name="user" {...user} />,
          permissions === 'admin' && 
            <Resource options={{ label: 'Đề thi' }} name="exam" {...exam} />,
          permissions === 'admin' && 
          <Resource options={{ label: 'Môn học' }} name="subject" {...subject} />,
          permissions === 'admin' && 
          <Resource options={{ label: 'Chuyên đề' }} name="thematic" {...thematic} />,
          permissions === 'admin' && 
          <Resource options={{ label: 'Câu hỏi' }} name="question" {...question} />,
          permissions === 'admin' && 
          <Resource options={{ label: 'Phòng thi hệ thống' }} name="systemRoom" {...systemRoom} />,
          permissions === 'admin' && 
          <Resource options={{ label: 'Lịch sử' }} name="history" {...history} />,
        ]}
      </Admin>
    );
  }
}

export default App;