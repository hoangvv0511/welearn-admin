import HistoryIcon from '@material-ui/icons/Restore';
import HistoryList from './list';

export default {
    list: HistoryList,
    icon: HistoryIcon,
};