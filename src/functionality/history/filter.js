import React from 'react';
import {
    Filter,
    ReferenceInput,
    SelectInput,
    DateTimeInput
} from 'react-admin';

const HistoryFilter = (props) => (
    <Filter {...props}>
        <DateTimeInput label="Từ" source="dateFrom" alwaysOn />
        <DateTimeInput label="Đến" source="dateTo" alwaysOn />
        <ReferenceInput label="User" source="user_id" reference="user" alwaysOn>
            <SelectInput optionText="name" />
        </ReferenceInput>
        <ReferenceInput label="Đề thi" source="exam_id" reference="exam" alwaysOn>
            <SelectInput optionText="name" />
        </ReferenceInput>
    </Filter>
);

export default HistoryFilter;