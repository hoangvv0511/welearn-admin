import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    DateField,
    CardActions,
    ReferenceField
} from 'react-admin';
import HistoryFilter from './filter';

const HistoryListActions = () => (
    <CardActions />
);

const HistoryList = props => {
    return (
        <List
            filters={<HistoryFilter />}
            bulkActionButtons={false}
            title="Lịch sử"
            actions={<HistoryListActions />}
            {...props}
        >
            <Datagrid>
                <TextField label="Id" source="id" />
                <ReferenceField linkType="" label="Tên User" source="user_id" reference="user">
                    <TextField source="name" />
                </ReferenceField>
                <ReferenceField linkType="" label="Tên đề thi" source="exam_id" reference="exam">
                    <TextField source="name" />
                </ReferenceField>
                <DateField label="Thời gian bắt đầu làm bài" source="start_time" showTime />
                <DateField label="Thời gian kết thúc làm bài" source="end_time" showTime />
            </Datagrid>
        </List>
    )
};

export default HistoryList;