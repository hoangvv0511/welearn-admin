import React from 'react';

import {
    Edit,
    SimpleForm,
    required,
    SelectInput,
    CardActions,
    ListButton,
    Toolbar,
    SaveButton,
    ReferenceInput,
    FormDataConsumer,
    TextInput
} from 'react-admin';

const QuestionEditTitle = ({ record }) => {
    return <span>Câu {record ? `${record.question}` : ''}</span>;
};

const answer = [
    { id: "A", name: "A" },
    { id: "B", name: "B" },
    { id: "C", name: "C" },
    { id: "D", name: "D" }
]

const difficulty = [
    { id: 0, name: "0" },
    { id: 1, name: "Nhận biết" },
    { id: 2, name: "Thông hiểu" },
    { id: 3, name: "Vận dụng" },
    { id: 4, name: "Vận dụng cao" },
]

const QuestionEditActions = ({ basePath }) => {
    return (
        <CardActions>
            <ListButton label="" basePath={basePath} />
        </CardActions>
    );
}

const QuestionEditToolbar = props =>
    <Toolbar {...props}>
        <SaveButton
            label="Lưu"
            redirect="/question"
            submitOnEnter={false}
        />
    </Toolbar>;

const QuestionEdit = props => {
    return (
        <Edit
            title={<QuestionEditTitle />}
            actions={<QuestionEditActions />}
            undoable={false}
            {...props}
        >
            <SimpleForm toolbar={<QuestionEditToolbar />}>
                <TextInput label="Id" source="id" disabled/>
                <TextInput label="Câu" source="question" disabled/>
                <SelectInput choices={answer} label="Đáp án" source="key_answer" validate={required()} />
                <FormDataConsumer>
                    {({ formData, ...rest }) => (
                        <ReferenceInput
                            label="Chuyên đề"
                            source="thematic_id"
                            reference="thematic"
                            perPage={0}
                            filter={{ id: formData.thematic_id }}
                            sort={{ field: 'subject_id', order: 'ASC' }}
                            fullWidth
                            validate={required()}
                            {...rest}
                        >
                            <SelectInput optionText="name" validate={required()}/>
                        </ReferenceInput>
                    )}
                </FormDataConsumer>
                <SelectInput choices={difficulty} label="Độ khó" source="difficulty" validate={required()} />
            </SimpleForm>
        </Edit>
    );
}

export default QuestionEdit;