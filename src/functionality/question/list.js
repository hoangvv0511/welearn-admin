import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    EditButton,
    FunctionField,
    Pagination,
    CardActions,
    CreateButton
} from 'react-admin';

import DeleteButton from '../customize/DeleteButton';
import QuestionFilter from './filter';

const difficulty = (record) => {
    switch (record.difficulty) {
        case 1:
            return "Nhận biết";
        case 2:
            return "Thông hiểu";
        case 3:
            return "Vận dụng";
        case 4:
            return "Vận dụng cao";
        default:
            return `${record.difficulty}`;
    }
};

const QuestionListActions = ({ basePath }) => (
    <CardActions>
        <CreateButton basePath={basePath} label="" />
    </CardActions>
);

const QuestionPagination = props => <Pagination rowsPerPageOptions={[40, 50]} {...props} />;

const QuestionList = props => (
    <List
        filters={<QuestionFilter />}
        bulkActionButtons={false}
        title="Câu hỏi"
        actions={<QuestionListActions />}
        pagination={<QuestionPagination />}
        perPage={50}
        sort={{ field: 'question', order: 'ASC' }}
        {...props}
    >
        <Datagrid>
            <TextField label="Mã đề thi" source="exam_id" />
            <TextField label="Câu" source="question" />
            <TextField label="Đáp án" source="key_answer" />
            <TextField label="Chuyên đề" source="question_thematic.name" />
            <FunctionField label="Độ khó" render={difficulty} source="difficulty" />
            <TextField label="Môn học" source="thematic_subject.name" />
            <EditButton label="" />
            <DeleteButton />
        </Datagrid>
    </List>
)

export default QuestionList;