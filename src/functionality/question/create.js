import React from 'react';
import {
    Create,
    SimpleForm,
    required,
    ReferenceInput,
    SelectInput,
    Toolbar,
    SaveButton,
    CardActions,
    ListButton,
    FormDataConsumer
} from 'react-admin';

const answer = [
    { id: "A", name: "A" },
    { id: "B", name: "B" },
    { id: "C", name: "C" },
    { id: "D", name: "D" }
]

const difficulty = [
    { id: 0, name: "0" },
    { id: 1, name: "Nhận biết" },
    { id: 2, name: "Thông hiểu" },
    { id: 3, name: "Vận dụng" },
    { id: 4, name: "Vận dụng cao" },
]

const QuestionCreateActions = ({ basePath }) => (
    <CardActions>
        <ListButton label="" basePath={basePath} />
    </CardActions>
);

const QuestionCreateToolbar = props =>
    <Toolbar {...props}>
        <SaveButton
            label="Tạo"
            redirect="/question"
            submitOnEnter={false}
        />
    </Toolbar>;

const QuestionCreate = props => (
    <Create
        actions={<QuestionCreateActions />}
        title="Tạo mới câu hỏi"
        {...props}
    >
        <SimpleForm toolbar={<QuestionCreateToolbar />} >
            <FormDataConsumer>
                {({ formData, ...rest }) => (
                    <ReferenceInput
                        label="Môn học"
                        source="subject_id"
                        reference="subject"
                        perPage={0}
                        fullWidth
                        validate={required()}
                        {...rest}
                    >
                        <SelectInput optionText="name" validate={required()} />
                    </ReferenceInput>
                )}
            </FormDataConsumer>
            <FormDataConsumer>
                {({ formData, ...rest }) => (
                    <ReferenceInput
                        label="Đề thi"
                        source="exam_id"
                        reference="exam"
                        perPage={0}
                        filter={{ subject_id: formData.subject_id }}
                        sort={{ field: 'subject_id', order: 'ASC' }}
                        fullWidth
                        validate={required()}
                        {...rest}
                    >
                        <SelectInput optionText="name" validate={required()} />
                    </ReferenceInput>
                )}
            </FormDataConsumer>
            <FormDataConsumer>
                {({ formData, ...rest }) => (
                    <ReferenceInput
                        label="Chuyên đề"
                        source="thematic_id"
                        reference="thematic"
                        perPage={0}
                        filter={{ exam_id: formData.exam_id }}
                        sort={{ field: 'subject_id', order: 'ASC' }}
                        fullWidth
                        validate={required()}
                        {...rest}
                    >
                        <SelectInput optionText="name" validate={required()} />
                    </ReferenceInput>
                )}
            </FormDataConsumer>
            <SelectInput choices={answer} label="Đáp án" source="key_answer" validate={required()} />
            <SelectInput choices={difficulty} label="Độ khó" source="difficulty" validate={required()} />
        </SimpleForm>
    </Create>
);

export default QuestionCreate;