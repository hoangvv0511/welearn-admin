import QuestionIcon from '@material-ui/icons/QuestionAnswer';
import QuestionList from './list';
import QuestionCreate from './create';
import QuestionEdit from './edit';

export default {
    list: QuestionList,
    create: QuestionCreate,
    edit: QuestionEdit,
    icon: QuestionIcon,
};