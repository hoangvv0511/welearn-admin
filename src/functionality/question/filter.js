import React from 'react';
import {
    Filter,
    TextInput
} from 'react-admin';

const QuestionFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Mã đề thi" source="exam_id" alwaysOn />
    </Filter>
);

export default QuestionFilter;