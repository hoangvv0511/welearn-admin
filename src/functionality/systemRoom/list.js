import React from 'react';
import SystemRoomFilter from './filter';
import {
    List,
    Datagrid,
    TextField,
    EditButton,
    ShowButton,
    CardActions,
    CreateButton,
    DateField,
    ReferenceField
} from 'react-admin';
import DeleteButton from '../customize/DeleteButton'

const SystemRoomListActions = ({ basePath }) => (
    <CardActions>
        <CreateButton basePath={basePath} label="" />
    </CardActions>
);

const SystemRoomList = props => (
    <List
        filters={<SystemRoomFilter />}
        bulkActionButtons={false}
        title="Phòng thi hệ thống"
        actions={<SystemRoomListActions />}
        sort={{ field: 'id', order: 'ASC' }}
        {...props}
    >
        <Datagrid>
            <TextField label="Mã phòng thi" source="id" />
            <TextField label="Tên phòng thi" source="name" />
            <DateField label="Thời gian bắt đầu" source="start_time" showTime />
            <ReferenceField linkType="" label="Môn học" source="systemRoom_exam.subject_id" reference="subject">
                <TextField source="name" />
            </ReferenceField>
            <TextField label="Tên đề thi" source="systemRoom_exam.name" />
            <EditButton label="" />
            <DeleteButton />
            <ShowButton label="" />
        </Datagrid>
    </List>
)

export default SystemRoomList;