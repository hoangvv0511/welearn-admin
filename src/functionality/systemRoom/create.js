import React from 'react';
import {
    Create,
    SimpleForm,
    TextInput,
    required,
    ReferenceInput,
    SelectInput,
    Toolbar,
    SaveButton,
    CardActions,
    ListButton,
    DateTimeInput,
    FormDataConsumer
} from 'react-admin';

const SystemRoomCreateActions = ({ basePath }) => (
    <CardActions>
        <ListButton label="" basePath={basePath} />
    </CardActions>
);

const SystemRoomCreateToolbar = props =>
    <Toolbar {...props}>
        <SaveButton
            label="Tạo"
            redirect="/systemRoom"
            submitOnEnter={false}
        />
    </Toolbar>;

const SystemRoomCreate = props => (
    <Create
        actions={<SystemRoomCreateActions />}
        title="Tạo mới phòng thi"
        {...props}
    >
        <SimpleForm toolbar={<SystemRoomCreateToolbar />} >
            <TextInput label="Tên phòng thi" source="name" validate={required()} />
            <FormDataConsumer>
                {({ formData, ...rest }) => (
                    <ReferenceInput
                        label="Môn học"
                        source="subject_id"
                        reference="subject"
                        perPage={0}
                        sort={{ field: 'subject_id', order: 'ASC' }}
                        fullWidth
                        validate={required()}
                        {...rest}
                    >
                        <SelectInput optionText="name" validate={required()} />
                    </ReferenceInput>
                )}
            </FormDataConsumer>
            <DateTimeInput label="Thời gian bắt đầu" source="start_time" validate={required()} />
            <FormDataConsumer>
                {({ formData, ...rest }) => (
                    <ReferenceInput
                        label="Đề thi"
                        source="exam_id"
                        reference="exam"
                        perPage={0}
                        filter={{ subject_id: formData.subject_id, is_online: true, is_exists : true }}
                        sort={{ field: 'subject_id', order: 'ASC' }}
                        fullWidth
                        validate={required()}
                        {...rest}
                    >
                        <SelectInput optionText="name" validate={required()} />
                    </ReferenceInput>
                )}
            </FormDataConsumer>
        </SimpleForm>
    </Create>
);

export default SystemRoomCreate;