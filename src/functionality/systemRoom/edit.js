import React from 'react';
import {
    Edit,
    SimpleForm,
    TextInput,
    required,
    CardActions,
    ListButton,
    Toolbar,
    SaveButton,
    DateTimeInput,
    ReferenceInput,
    SelectInput,
    FormDataConsumer
} from 'react-admin';

const SystemRoomEditTitle = ({ record }) => {
    return <span>{record ? `${record.name}` : ''}</span>;
};

const SystemRoomEditActions = ({ basePath }) => (
    <CardActions>
        <ListButton label="" basePath={basePath} />
    </CardActions>
);

const SystemRoomEditToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="Lưu"
            redirect="/systemRoom"
            submitOnEnter={false}
        />
    </Toolbar>
)

const SystemRoomEdit = props => (
    <Edit
        title={<SystemRoomEditTitle />}
        actions={<SystemRoomEditActions />}
        undoable={false}
        {...props}
    >
        <SimpleForm toolbar={<SystemRoomEditToolbar />}>
            <TextInput label="Id" source="id" disabled />
            <TextInput label="Tên phòng thi" source="name" validate={required()} />
            <DateTimeInput label="Thời gian bắt đầu" source="start_time" showTime validate={required()} />
            <FormDataConsumer>
                {({ formData, ...rest }) => (
                    <ReferenceInput
                        label="Môn học"
                        source="subject_id"
                        reference="subject"
                        perPage={0}
                        sort={{ field: 'subject_id', order: 'ASC' }}
                        fullWidth
                        {...rest}
                    >
                        <SelectInput optionText="name" />
                    </ReferenceInput>
                )}
            </FormDataConsumer>
            <FormDataConsumer>
                {({ formData, ...rest }) => (
                    <ReferenceInput
                        label="Đề thi"
                        source="exam_id"
                        reference="exam"
                        perPage={0}
                        filter={{ subject_id: formData.subject_id, is_online: true, is_exists : true }}
                        sort={{ field: 'subject_id', order: 'ASC' }}
                        fullWidth
                        validate={required()}
                        {...rest}
                    >
                        <SelectInput optionText="name" validate={required()} />
                    </ReferenceInput>
                )}
            </FormDataConsumer>
        </SimpleForm>
    </Edit>
);

export default SystemRoomEdit;