import SystemRoomIcon from '@material-ui/icons/RssFeed';
import SystemRoomList from './list';
import SystemRoomCreate from './create';
import SystemRoomEdit from './edit';
import SystemRoomShow from './show';

export default {
    list: SystemRoomList,
    create: SystemRoomCreate,
    edit: SystemRoomEdit,
    icon: SystemRoomIcon,
    show: SystemRoomShow
};