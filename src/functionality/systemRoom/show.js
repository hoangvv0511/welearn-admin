import React from 'react';
import {
    Show,
    SimpleShowLayout,
    TextField,
    CardActions,
    ListButton,
    EditButton,
    DateField
} from 'react-admin';

const ExamShowTitle = ({ record }) => {
    return <span>{record ? `${record.name}` : ''}</span>;
};

const ExamShowActions = ({ basePath, data }) => (
    <CardActions>
        <ListButton label="" basePath={basePath} />
        <EditButton label="" basePath={basePath} record={data} />
    </CardActions>
);

export const ExamShow = props => (
    <Show
        actions={<ExamShowActions />}
        title={<ExamShowTitle />}
        {...props}
    >
        <SimpleShowLayout>
            <TextField label="Id" source="id" />
            <TextField label="Tên phòng thi" source="name" />
            <DateField label="Thời gian bắt đầu" source="start_time" showTime />
            <TextField label="Id đề thi" source="exam_id" />
            <TextField label="Users" source="users" />
            <DateField label="Thời gian tạo phòng thi" source="create_at" />
        </SimpleShowLayout>
    </Show>
);

export default ExamShow;