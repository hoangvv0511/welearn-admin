import React from 'react';
import {
    Filter,
    TextInput,
    ReferenceInput,
    SelectInput
} from 'react-admin';

const SystemRoomFilterExam = [
    { id: "not_open", name: "Chưa thi" },
    { id: "openning", name: "Đang thi" }
]

const SystemRoomFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Id phòng thi" source="id" alwaysOn />
        <TextInput label="Tên phòng thi" source="name" alwaysOn />
        <ReferenceInput label="Môn học" source="subject_id" reference="subject" alwaysOn>
            <SelectInput optionText="name" />
        </ReferenceInput>
        <SelectInput 
            label="Trạng thái đề thi"
            choices={SystemRoomFilterExam}
            source="is_open"
            alwaysOn
            />
    </Filter>
);

export default SystemRoomFilter;