import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK, AUTH_GET_PERMISSIONS } from 'react-admin';
const jwt = require('jsonwebtoken');
const secretOrKey = require('../../config/constants').secretOrKey;
const lifeRefreshToken = require('../../config/constants').lifeRefreshToken;

export default (type, params) => {
    // called when the user attempts to log in
    if (type === AUTH_LOGIN) {
        const { username, password } = params;
        const request = new Request('https://welearn-api.herokuapp.com/authenticate', {
            method: 'POST',
            body: JSON.stringify({ username, password }),
            headers: new Headers({ 'Content-Type': 'application/json' }),
        })
        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(({ token, role }) => {
                localStorage.setItem('token', token);
                localStorage.setItem('role', role);
            });
    }
    if (type === AUTH_LOGOUT) {
        localStorage.removeItem('token');
        localStorage.removeItem('role');
        return Promise.resolve();
    }
    if (type === AUTH_ERROR) {
        const status = params.status;
        if (status === 401 || status === 403) {
            localStorage.removeItem('token');
            localStorage.removeItem('role');
            return Promise.reject();
        }
        return Promise.resolve();
    }
    if (type === AUTH_CHECK) {
        const token = localStorage.getItem('token');
        var exp;    // time end token
        jwt.verify(token, secretOrKey, (err, payload) => {
            if (payload) {
                exp = payload.exp;
            } else {
                // Nếu token tồn tại nhưng không hợp lệ, server sẽ response status code 401 với msg bên dưới
                return Promise.reject();
            }
        })
        var request = new Request('https://welearn-api.herokuapp.com/refreshToken', {
            method: 'GET',
            headers: new Headers({ 'Content-Type': 'application/json', 'Authorization': token }),
        });
        const dateNow = Date.now();
        if (exp - dateNow / 1000 <= lifeRefreshToken) {
            console.log("refreshToken");
            console.log(localStorage.getItem('token'));
            fetch(request)
                .then(response => {
                    if (response.status < 200 || response.status >= 300) {
                        throw new Error(response.statusText);
                    }
                    return response.json();
                })
                .then(({ refreshToken }) => {
                    localStorage.setItem('token', refreshToken);
                    console.log(localStorage.getItem('token'));
                });

        }

        return localStorage.getItem('token') ? Promise.resolve() : Promise.reject();
    }
    if (type === AUTH_GET_PERMISSIONS) {
        const role = localStorage.getItem('role');
        return role ? Promise.resolve(role) : Promise.reject();
    }
    return Promise.reject('Unknown method');
};