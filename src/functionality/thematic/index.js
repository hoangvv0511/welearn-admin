import ThematicIcon from '@material-ui/icons/ClearAll';
import ThematicList from './list';
import ThematicCreate from './create';
import ThematicEdit from './edit';

export default {
    list: ThematicList,
    create: ThematicCreate,
    edit: ThematicEdit,
    icon: ThematicIcon,
};