import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    EditButton,
    ReferenceField,
    CardActions,
    CreateButton
} from 'react-admin';
import DeleteButton from '../customize/DeleteButton'
import ThematicFilter from './filter';

const ThematicListActions = ({ basePath }) => (
    <CardActions>
        <CreateButton basePath={basePath} label="" />
    </CardActions>
);

const ThematicList = props => (
    <List
        filters={<ThematicFilter />}
        bulkActionButtons={false}
        title="Chuyên đề"
        actions={<ThematicListActions />}
        {...props}
    >
        <Datagrid>
            <TextField label="STT" source="STT" sortable={false} />
            <TextField label="Tên chuyên đề" source="name" />
            <ReferenceField linkType="" label="Môn học" source="subject_id" reference="subject">
                <TextField source="name" />
            </ReferenceField>
            <EditButton label="" />
            <DeleteButton />
        </Datagrid>
    </List>
)

export default ThematicList;