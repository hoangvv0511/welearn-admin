import React from 'react';
import {
    CardActions,
    ListButton,
    Create,
    SimpleForm,
    TextInput,
    required,
    ReferenceInput,
    SelectInput,
    Toolbar,
    SaveButton
} from 'react-admin';

const ThematicCreateActions = ({ basePath }) => (
    <CardActions>
        <ListButton label="" basePath={basePath} />
    </CardActions>
);

const ThematicCreateToolbar = props =>
    <Toolbar {...props}>
        <SaveButton
            label="Tạo"
            redirect="/thematic"
            submitOnEnter={false}
        />
    </Toolbar>;

const ThematicCreate = props => (
    <Create
        actions={<ThematicCreateActions />}
        title="Tạo mới chuyên đề"
        {...props}
    >
        <SimpleForm toolbar={<ThematicCreateToolbar />} >
            <TextInput label="Tên chuyên đề" source="name" validate={required()} />
            <ReferenceInput label="Môn học" source="subject_id" reference="subject" validate={required()}>
                <SelectInput optionText="name" validate={required()} />
            </ReferenceInput>
            <TextInput label="Id chuyên đề" source="id" validate={required()} />
        </SimpleForm>
    </Create>
);

export default ThematicCreate;