import React from 'react';
import {
    Filter,
    TextInput,
    ReferenceInput,
    SelectInput
} from 'react-admin';

const ThematicFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Tên chuyên đề" source="name" alwaysOn />
        <ReferenceInput label="Môn học" source="subject_id" reference="subject" alwaysOn>
            <SelectInput optionText="name" />
        </ReferenceInput>
    </Filter>
);

export default ThematicFilter;