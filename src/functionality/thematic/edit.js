import React from 'react';
import {
    CardActions,
    ListButton,
    Toolbar,
    SaveButton,
    Edit,
    SimpleForm,
    TextInput,
    required,
    ReferenceInput,
    SelectInput
} from 'react-admin';

const ThematicEditTitle = ({ record }) => {
    return <span>{record ? `${record.name}` : ''}</span>;
};

const ThematicEditActions = ({ basePath }) => (
    <CardActions>
        <ListButton label="" basePath={basePath} />
    </CardActions>
);

const ThematicEditToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="Lưu"
            redirect="/thematic"
            submitOnEnter={false}
        />
    </Toolbar>
)

const ThematicEdit = props => (
    <Edit
        title={<ThematicEditTitle />}
        actions={<ThematicEditActions />}
        undoable={false}
        {...props}
    >
        <SimpleForm toolbar={<ThematicEditToolbar />}>
            <TextInput label="Id chuyên đề" source="id" disabled />
            <TextInput label="Tên chuyên đề" source="name" validate={required()} />
            <ReferenceInput label="Môn học" source="subject_id" reference="subject" validate={required()}>
                <SelectInput optionText="name" validate={required()} />
            </ReferenceInput>
        </SimpleForm>
    </Edit>
);

export default ThematicEdit;