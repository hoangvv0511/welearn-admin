import React from 'react';
import {
    Edit,
    SimpleForm,
    TextInput,
    required,
    CardActions,
    ListButton,
    Toolbar,
    SaveButton
} from 'react-admin';

const SubjectEditTitle = ({ record }) => {
    return <span>{record ? `${record.name}` : ''}</span>;
};

const SubjectEditActions = ({ basePath }) => (
    <CardActions>
        <ListButton label="" basePath={basePath} />
    </CardActions>
);

const SubjectEditToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="Lưu"
            redirect="/subject"
            submitOnEnter={false}
        />
    </Toolbar>
)

const SubjectEdit = props => (
    <Edit
        title={<SubjectEditTitle />}
        actions={<SubjectEditActions />}
        undoable={false}
        {...props}
    >
        <SimpleForm toolbar={<SubjectEditToolbar />}>
            <TextInput label="Tên môn học" source="name" validate={required()} />
        </SimpleForm>
    </Edit>
);

export default SubjectEdit;