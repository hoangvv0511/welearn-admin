import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    EditButton,
    CardActions,
    Pagination
} from 'react-admin';
import SubjectFilter from './filter';

const SubjectListActions = () => (
    <CardActions/>
);

const SubjectListPagination = props => <Pagination rowsPerPageOptions={[]} {...props} />;

const SubjectList = props => {
    return (
        <List
            filters={<SubjectFilter />}
            bulkActionButtons={false}
            title="Môn học"
            actions={<SubjectListActions />}
            pagination={<SubjectListPagination />}
            {...props}
        >
            <Datagrid>
                <TextField label="STT" source="STT" sortable={false} />
                <TextField label="Tên môn học" source="name" sortable={false} />
                <EditButton label="" />
            </Datagrid>
        </List>
    )
};

export default SubjectList;