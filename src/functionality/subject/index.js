import SubjectIcon from '@material-ui/icons/ViewList';
import SubjectList from './list';
import SubjectEdit from './edit';

export default {
    list: SubjectList,
    edit: SubjectEdit,
    icon: SubjectIcon,
};