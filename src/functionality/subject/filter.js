import React from 'react';
import {
    Filter,
    TextInput
} from 'react-admin';

const SubjectFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Tên môn học" source="name" alwaysOn allowEmpty />
    </Filter>
);

export default SubjectFilter;