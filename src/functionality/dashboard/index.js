import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';

export default () => (
    <Card>
        <CardHeader title="Xin chào mừng đến với welearn-admin" />
        <CardContent>Xin chào mừng đến với welearn-admin...</CardContent>
    </Card>
);