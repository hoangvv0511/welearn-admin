import React from 'react';
import {
    Show,
    SimpleShowLayout,
    TextField,
    ReferenceField,
    CardActions,
    ListButton,
    EditButton,
    BooleanField,
    DateField
} from 'react-admin';

const ExamShowTitle = ({ record }) => {
    return <span>{record ? `${record.name}` : ''}</span>;
};

const ExamShowActions = ({ basePath, data }) => (
    <CardActions>
        <ListButton label="" basePath={basePath} />
        <EditButton label="" basePath={basePath} record={data} />
    </CardActions>
);

export const ExamShow = props => (
    <Show
        actions={<ExamShowActions />}
        title={<ExamShowTitle />}
        {...props}
    >
        <SimpleShowLayout>
            <TextField label="Id" source="id" />
            <TextField label="Tên đề thi" source="name" />
            <ReferenceField linkType="" label="Môn học" source="subject_id" reference="subject">
                <TextField source="name" />
            </ReferenceField>
            <TextField label="Đề thi" source="question" />
            <TextField label="Đáp án" source="answer" />
            <TextField label="Thời gian làm bài(phút)" source="exam_time" />
            <BooleanField label="Online" source="is_online" />
            <TextField label="Năm" source="year" />
            <DateField label="Thời gian tạo đề" source="create_at" />
            <TextField label="Câu bắt đầu" source="start_id" />
            <TextField label="Số lần làm bài" source="count" />
            <TextField label="Tổng số sao của đề thi" source="star" />
            <TextField label="Số lượng câu" source="number_of_question" />
        </SimpleShowLayout>
    </Show>
);

export default ExamShow;