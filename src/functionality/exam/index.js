import ExamIcon from '@material-ui/icons/Assignment';
import ExamList from './list';
import ExamCreate from './create';
import ExamEdit from './edit';
import ExamShow from './show';

export default {
    list: ExamList,
    create: ExamCreate,
    edit: ExamEdit,
    icon: ExamIcon,
    show: ExamShow
};