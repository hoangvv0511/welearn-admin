import React from 'react';
import ExamFilter from './filter';
import {
    List,
    Datagrid,
    TextField,
    EditButton,
    ReferenceField,
    ShowButton,
    CardActions,
    CreateButton,
    BooleanField,
    DateField
} from 'react-admin';
import DeleteButton from '../customize/DeleteButton'

const ExamListActions = ({ basePath }) => (
    <CardActions>
        <CreateButton basePath={basePath} label="" />
    </CardActions>
);

const ExamList = props => (
    <List
        filters={<ExamFilter />}
        bulkActionButtons={false}
        title="Đề thi"
        actions={<ExamListActions />}
        sort={{ field: 'id', order: 'ASC' }}
        {...props}
    >
        <Datagrid>
            <TextField label="Mã đề thi" source="id" />
            <TextField label="Tên đề thi" source="name" />
            <ReferenceField linkType="" label="Môn học" source="subject_id" reference="subject">
                <TextField source="name" />
            </ReferenceField>
            <BooleanField label="Online" source="is_online" />
            <DateField label="Thời gian tạo đề" source="create_at" />
            <TextField label="Năm" source="year" />
            <EditButton label="" />
            <DeleteButton />
            <ShowButton label="" />
        </Datagrid>
    </List>
)

export default ExamList;