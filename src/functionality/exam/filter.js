import React from 'react';
import {
    Filter,
    TextInput,
    ReferenceInput,
    SelectInput,
    NumberInput
} from 'react-admin';

const ExamFilterOnline = [
    { id: "true", name: "Có" },
    { id: "false", name: "Không" }
]

const ExamFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Tên đề thi" source="name" alwaysOn />
        <ReferenceInput label="Môn học" source="subject_id" reference="subject" alwaysOn>
            <SelectInput optionText="name" />
        </ReferenceInput>
        <SelectInput choices={ExamFilterOnline} label="Online" source="is_online" alwaysOn />
        <NumberInput label="Năm" source="year" alwaysOn />
    </Filter>
);

export default ExamFilter;