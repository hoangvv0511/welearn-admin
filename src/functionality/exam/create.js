import React from 'react';
import {
    Create,
    SimpleForm,
    TextInput,
    required,
    ReferenceInput,
    SelectInput,
    Toolbar,
    SaveButton,
    CardActions,
    ListButton,
    NumberInput
} from 'react-admin';

const ExamCreateActions = ({ basePath }) => (
    <CardActions>
        <ListButton label="" basePath={basePath} />
    </CardActions>
);

const ExamCreateToolbar = props =>
    <Toolbar {...props}>
        <SaveButton
            label="Tạo"
            redirect="/exam"
            submitOnEnter={false}
        />
    </Toolbar>;

const ExamCreateOnline = [
    { id: "true", name: "Có" },
    { id: "false", name: "Không" }
]

const ExamCreate = props => (
    <Create
        actions={<ExamCreateActions />}
        title="Tạo mới đề thi"
        {...props}
    >
        <SimpleForm toolbar={<ExamCreateToolbar />} >
            <TextInput label="Tên đề thi" source="name" validate={required()} />
            <ReferenceInput label="Môn học" source="subject_id" reference="subject" >
                <SelectInput optionText="name" validate={required()} />
            </ReferenceInput>
            <TextInput label="Đề thi" source="question" validate={required()} />
            <TextInput label="Đáp án" source="answer" validate={required()} />
            <NumberInput label="Thời gian làm bài(phút)" source="exam_time" validate={required()} />
            <SelectInput choices={ExamCreateOnline} label="Online" source="is_online" validate={required()} />
            <NumberInput label="Năm" source="year" validate={required()} />
            <NumberInput label="Câu bắt đầu" source="start_id" validate={required()} />
            <NumberInput label="Số lượng câu" source="number_of_question" />
        </SimpleForm>
    </Create>
);

export default ExamCreate;