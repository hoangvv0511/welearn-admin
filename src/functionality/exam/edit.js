import React from 'react';
import {
    Edit,
    SimpleForm,
    TextInput,
    required,
    ReferenceInput,
    SelectInput,
    CardActions,
    ListButton,
    Toolbar,
    SaveButton,
    NumberInput
} from 'react-admin';

const ExamEditTitle = ({ record }) => {
    return <span>{record ? `${record.name}` : ''}</span>;
};

const ExamEditActions = ({ basePath }) => (
    <CardActions>
        <ListButton label="" basePath={basePath} />
    </CardActions>
);

const ExamEditToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="Lưu"
            redirect="/exam"
            submitOnEnter={false}
        />
    </Toolbar>
)

const isOnline = [
    { id: "true", name: "Có" },
    { id: "false", name: "Không" }
]

const ExamEdit = props => (
    <Edit
        title={<ExamEditTitle />}
        actions={<ExamEditActions />}
        undoable={false}
        {...props}
    >
        <SimpleForm toolbar={<ExamEditToolbar />}>
            <TextInput label="Id" source="id" disabled />
            <TextInput label="Tên đề thi" source="name" validate={required()} />
            <ReferenceInput label="Môn học" source="subject_id" reference="subject" validate={required()}>
                <SelectInput optionText="name" validate={required()} />
            </ReferenceInput>
            <TextInput label="Đề thi" source="question" validate={required()} />
            <TextInput label="Đáp án" source="answer" validate={required()} />
            <NumberInput label="Thời gian làm bài(phút)" source="exam_time" validate={required()} />
            <SelectInput choices={isOnline} label="Online" source="is_online" validate={required()} />
            <NumberInput label="Năm" source="year" validate={required()} />
            <NumberInput label="Câu bắt đầu" source="start_id" validate={required()} />
        </SimpleForm>
    </Edit>
);

export default ExamEdit;