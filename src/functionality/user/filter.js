import React from 'react';
import {
    Filter,
    TextInput
} from 'react-admin';

const UserFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Tìm kiếm" source="q" alwaysOn />
    </Filter>
);

export default UserFilter;