import React from 'react';
import {
    List,
    Datagrid,
    TextField,
    EditButton,
    ShowButton,
    EmailField,
    CardActions
} from 'react-admin';
import DeleteButton from '../customize/DeleteButton'
import UserFilter from './filter';

const UserListActions = () => (
    <CardActions>
    </CardActions>
);

const UserList = props => (
    <List
        filters={<UserFilter />}
        bulkActionButtons={false}
        title="User"
        actions={<UserListActions />}
        sort={{ field: 'id', order: 'ASC' }}
        {...props}
    >
        <Datagrid>
            <TextField source="id" />
            <TextField source="name" />
            <EmailField source="email" />
            <EditButton label="" />
            <DeleteButton />
            <ShowButton label="" />
        </Datagrid>
    </List>
);

export default UserList;