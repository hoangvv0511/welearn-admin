import UserIcon from '@material-ui/icons/Group';
import UserList from './list';
import UserEdit from './edit';
import UserShow from './show';

export default {
    list: UserList,
    edit: UserEdit,
    icon: UserIcon,
    show: UserShow
};