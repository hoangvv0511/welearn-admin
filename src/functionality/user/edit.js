import React from 'react';
import {
    Edit,
    SimpleForm,
    TextInput,
    required,
    CardActions,
    ListButton,
    Toolbar,
    SaveButton
} from 'react-admin';

const UserEditTitle = ({ record }) => {
    return <span>{record ? `${record.name}` : ''}</span>;
};

const UserEditActions = ({ basePath }) => (
    <CardActions>
        <ListButton label="" basePath={basePath} />
    </CardActions>
);

const UserEditToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="Lưu"
            redirect="/user"
            submitOnEnter={false}
        />
    </Toolbar>
)

const UserEdit = props => (
    <Edit
        title={<UserEditTitle />}
        actions={<UserEditActions />}
        undoable={false}
        {...props}
    >
        <SimpleForm toolbar={<UserEditToolbar />}>
            <TextInput label="Ảnh" source="image" validate={required()} />
            <TextInput label="Tên" source="name" validate={required()} />
        </SimpleForm>
    </Edit>
);

export default UserEdit;