import React from 'react';
import {
    Show,
    SimpleShowLayout,
    TextField,
    CardActions,
    ListButton,
    EditButton
} from 'react-admin';

const UserShowTitle = ({ record }) => {
    return <span>{record ? `${record.name}` : ''}</span>;
};

const UserShowActions = ({ basePath, data }) => (
    <CardActions>
        <ListButton label="" basePath={basePath} />
        <EditButton label="" basePath={basePath} record={data} />
    </CardActions>
);

export const UserShow = props => (
    <Show
        actions={<UserShowActions />}
        title={<UserShowTitle />}
        {...props}>
        <SimpleShowLayout>
            <TextField source="id" />
            <TextField source="image" />
            <TextField source="name" />
            <TextField source="email" />
            <TextField source="history" />
        </SimpleShowLayout>
    </Show>
);

export default UserShow;